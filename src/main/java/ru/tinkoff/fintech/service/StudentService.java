package ru.tinkoff.fintech.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.dao.StudentRepository;
import ru.tinkoff.fintech.model.Student;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class StudentService {

    private final StudentRepository repository;

    public void save(Student student) {
        repository.save(student);
    }

    public Student findById(UUID id) {
        return repository.findById(id).orElseThrow();
    }

    public List<Student> findAll(List<UUID> ids) {
        return repository.findAll(ids);
    }

    public void update(Student student) {
        repository.update(student);
    }

    public void delete(UUID id) {
        repository.delete(id);
    }

    public void addToCourse(UUID courseId, List<UUID> studentsId) {
        repository.addToCourse(courseId, studentsId);
    }
}
