package ru.tinkoff.fintech.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.fintech.validation.GradeConstraint;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@GradeConstraint(groups = AddToCourseRequest.CustomGroup.class)
public class AddToCourseRequest {

    @NotNull
    UUID courseId;

    @NotEmpty
    List<UUID> studentIds;

    public interface CustomGroup {}

    @GroupSequence({Default.class, CustomGroup.class})
    public interface AddToCourseRequestValidationSequence {}
}
