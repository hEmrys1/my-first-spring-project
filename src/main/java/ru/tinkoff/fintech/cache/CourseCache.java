package ru.tinkoff.fintech.cache;

import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.model.Course;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CourseCache {

    private final ConcurrentHashMap<UUID, Course> cache = new ConcurrentHashMap<>();

    public void save(Course course) {
        cache.put(course.getId(), course);
    }

    public void update(Course course) {
        delete(course.getId());
        save(course);
    }

    public void delete(UUID id) {
        cache.remove(id);
    }

    public Optional<Course> get(UUID id) {
        Course courseToFind = cache.get(id);
        if (courseToFind == null) {
            return Optional.empty();
        }
        return Optional.of(courseToFind);
    }

    public void clear() {
        cache.clear();
    }
}
