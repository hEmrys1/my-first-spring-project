package ru.tinkoff.fintech.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Student {

    UUID id;
    String name;
    Integer age;
    Integer grade;
    List<UUID> courseIds;

    public static Mapper mapper = new Mapper();

    public Student(UUID id, String name, int age, int grade) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

    private static class Mapper implements RowMapper<Student> {

        @Override
        public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Student(
                    UUID.fromString(rs.getString("id")),
                    rs.getString("name"),
                    rs.getInt("age"),
                    rs.getInt("grade")
            );
        }
    }
}
