CREATE TABLE students
(
    id     UUID PRIMARY KEY,
    name   VARCHAR(64) NOT NULL,
    age    int         NOT NULL,
    course VARCHAR(64)
);
