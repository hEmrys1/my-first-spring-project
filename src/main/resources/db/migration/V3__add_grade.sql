ALTER TABLE students DROP CONSTRAINT FK_STUDENTS_COURSES;
DROP TABLE IF EXISTS courses;

CREATE TABLE courses
(
    id             LONG PRIMARY KEY NOT NULL,
    name           VARCHAR(64)      NOT NULL,
    description    VARCHAR(64),
    required_grade INT              NOT NULL
);

DROP TABLE IF EXISTS students;

CREATE TABLE students
(
    id        UUID PRIMARY KEY,
    name      VARCHAR(64) NOT NULL,
    age       INT         NOT NULL,
    grade     INT         NOT NULL,
    course_id LONG,
    CONSTRAINT FK_STUDENTS_COURSES FOREIGN KEY (course_id) REFERENCES courses (id)
);
