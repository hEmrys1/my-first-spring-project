ALTER TABLE students
    DROP CONSTRAINT FK_STUDENTS_COURSES;
DROP TABLE IF EXISTS courses;

CREATE TABLE courses
(
    id             UUID DEFAULT RANDOM_UUID() PRIMARY KEY,
    name           VARCHAR(200) NOT NULL,
    description    VARCHAR(800) NOT NULL,
    required_grade INT          NOT NULL
);

DROP TABLE IF EXISTS students;

CREATE TABLE students
(
    id    UUID DEFAULT RANDOM_UUID() PRIMARY KEY,
    name  VARCHAR(200) NOT NULL,
    age   INT          NOT NULL,
    grade INT          NOT NULL
);

CREATE TABLE student_course
(
    student_id UUID NOT NULL,
    course_id  UUID NOT NULL,
    PRIMARY KEY (student_id, course_id),
    CONSTRAINT FK_STUDENT_COURSE_STUDENT FOREIGN KEY (student_id) REFERENCES students (id) ON DELETE CASCADE,
    CONSTRAINT FK_STUDENT_COURSE_COURSE FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE CASCADE
);
