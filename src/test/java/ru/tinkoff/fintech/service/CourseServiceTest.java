package ru.tinkoff.fintech.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import ru.tinkoff.fintech.AbstractTest;
import ru.tinkoff.fintech.cache.CourseCache;
import ru.tinkoff.fintech.dao.CourseRepository;
import ru.tinkoff.fintech.model.Course;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
class CourseServiceTest extends AbstractTest {

    @Autowired
    private CourseCache cache;
    @Autowired
    private CourseService service;
    @Autowired
    private CourseRepository repository;

    @Test
    void testCourseNotInCacheSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        assertEquals(Optional.empty(), cache.get(courseId));
    }

    @Test
    void testSaveCourseSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        service.save(course);
        assertEquals(Optional.of(course), cache.get(courseId));
        assertEquals(Optional.of(course), repository.findById(courseId));
    }

    @Test
    void testGetCourseNotCachedSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        service.save(course);
        Course courseFound = service.findById(courseId);
        assertEquals(Optional.of(course), cache.get(courseId));
        assertEquals(Optional.of(course), repository.findById(courseId));
    }

    @Test
    void testGetCourseAlreadyCachedSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        service.save(course);
        Course courseFromCache = service.findById(courseId);
        assertEquals(Optional.of(course), cache.get(courseId));
        assertEquals(courseFromCache, service.findById(courseId));
    }

    @Test
    void testUpdateCourseSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        service.save(course);
        course.setDescription("Java Concurrency");
        course.setRequired_grade(5);
        service.update(course);
        assertEquals(Optional.of(course), cache.get(courseId));
    }

    @Test
    void testDeleteCourseSuccess() {
        Course course = prepareCourse(UUID.randomUUID());
        UUID courseId = course.getId();

        service.save(course);
        service.delete(courseId);
        assertEquals(Optional.empty(), cache.get(courseId));
    }

    private Course prepareCourse(UUID id) {
        return Course.builder()
                .id(id)
                .name("Java")
                .description("Fintech")
                .required_grade(4)
                .build();
    }
}